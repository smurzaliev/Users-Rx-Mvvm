//
//  UserCell.swift
//  Users-Rx-Mvvm
//
//  Created by Samat Murzaliev on 12.07.2022.
//

import UIKit
import SnapKit

class UserCell: UITableViewCell {
    
    private lazy var nameLbl: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 16, weight: .bold)
        view.textColor = .black
        return view
    }()
    
    private lazy var usernameLbl: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .medium)
        view.textColor = .black
        return view
    }()
    
    private lazy var emailLbl: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.textColor = .black
        return view
    }()
    
    private lazy var addressLbl: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.textColor = .black
        return view
    }()
    
    private lazy var companyLbl: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.textColor = .black
        return view
    }()
    
    override func layoutSubviews() {
        setupSubview()
    }
    
    private func setupSubview() {
        backgroundColor = .white
        addSubview(nameLbl)
        nameLbl.snp.makeConstraints { make in
            make.left.top.equalToSuperview().offset(5)
        }
        
        addSubview(usernameLbl)
        usernameLbl.snp.makeConstraints { make in
            make.top.equalTo(nameLbl.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(5)
        }
        
        addSubview(companyLbl)
        companyLbl.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(5)
            make.top.equalTo(usernameLbl.snp.bottom).offset(5)
        }
        
        addSubview(emailLbl)
        emailLbl.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(5)
            make.top.equalTo(companyLbl.snp.bottom).offset(5)
        }
        
        addSubview(addressLbl)
        addressLbl.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(5)
            make.top.equalTo(emailLbl.snp.bottom).offset(5)
        }
    }
    
    func fill(_ user: User) {
        nameLbl.text = "Name: \(user.name)"
        usernameLbl.text = "Username: \(user.username)"
        companyLbl.text = "Company: \(user.company.name)"
        emailLbl.text = "Email: \(user.email)"
        addressLbl.text = "City: \(user.address.city)"
    }
}
