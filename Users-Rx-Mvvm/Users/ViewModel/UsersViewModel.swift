//
//  UsersViewModel.swift
//  Users-Rx-Mvvm
//
//  Created by Samat Murzaliev on 12.07.2022.
//

import Foundation
import RxSwift
import RxCocoa

class UsersViewModel {
        
    func getusersList() -> Observable<Users> {
        let url = URL(string: "https://jsonplaceholder.typicode.com/users")!
        return Observable.create { observer -> Disposable in
            let task = URLSession.shared.dataTask(with: url) { (data, _ , error) in
                if error != nil {
                    print("Error receiving data: \(String(describing: error))")
                } else {
                    guard let data = data, let decoded = try? JSONDecoder().decode(Users.self, from: data) else {
                        return
                    }
                    observer.onNext(decoded)
                    observer.onCompleted()
                }
            }
            task.resume()
            return Disposables.create {
                task.cancel()
            }
        }
    }
}
