//
//  ViewController.swift
//  Users-Rx-Mvvm
//
//  Created by Samat Murzaliev on 12.07.2022.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class UsersViewController: UIViewController {
    
    private var disposeBag = DisposeBag()
    private let viewModel = UsersViewModel()

    private lazy var usersTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.register(UserCell.self, forCellReuseIdentifier: "UserCell")
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUsers()
        setupSubview()
    }
    
    private func getUsers() {
        viewModel.getusersList().bind(to: usersTable.rx
            .items(cellIdentifier: "UserCell", cellType: UserCell.self)){ row, user, cell in
                cell.fill(user)
            }
            .disposed(by: disposeBag)
    }
    
    private func setupSubview() {
        view.backgroundColor = .white
        view.addSubview(usersTable)
    
        usersTable.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension UsersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.bounds.height / 7
    }
}
